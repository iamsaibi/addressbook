import { Contact } from "./contact.model";

export class AddressBook {
  contacts: Contact[];

  constructor() {
    this.contacts = [];
  }

  add(contact: Contact) {
    this.contacts.push(contact);
  }

  edit(contact: Contact) {
    let index = this.contacts.findIndex((data) => data.id == contact.id);
    this.contacts.splice(index, 1, contact);
  }

  delete(contactId: number) {
    let index = this.contacts.findIndex((data) => data.id == contactId);
    if (index != -1) {
      this.contacts.splice(index, 1);
    }
    return;
  }
}
