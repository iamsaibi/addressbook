export class Contact {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  contactNumber: ContactNumber[];
  dateOfBirth: Date;

  constructor(id = 0) {
    this.id = id;
  }
}

export interface ContactNumber {
  countryCode: string;
  cellNumber: number;
  contactType: string;
}
