import { Injectable } from '@angular/core';
import { AddressBook } from '../model/address-book';
import { Contact } from '../model/contact.model';
import { setStorage, getFromStorage } from '../model/storage.utilities';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ContactProcessingService {
  contactEditSubject: Subject<number>;
  addressBook: AddressBook;
  constructor() {
    this.contactEditSubject = new Subject();
    this.addressBook = new AddressBook();
    this.addressBook.contacts = getFromStorage('contacts') ? getFromStorage('contacts') : [];
  }

  /**
 * * A method to consolidate form data into one object of type Contact

 * @param firstName Firstname
 * @param lastName Lastname
 * @param email Email
 * @param contactType Contact type: Home | Personal | Office
 * @param countryCode Country Code
 * @param cellNumber Mobile Number
 * @param dateOfBirth Date of Birth
 * @returns Contact object
 */
  consolidateData(
    firstName: string,
    lastName: string,
    email: string,
    phone: {
      contactType: string;
      countryCode: string;
      cellNumber: number;
    }[],
    dateOfBirth: Date
  ): Contact {
    let c = new Contact(this.addressBook.contacts.length + 1);
    c.firstName = firstName;
    c.lastName = lastName;
    c.email = email;
    phone.forEach((k) => {
      c.contactNumber = c.contactNumber || [];
      c.contactNumber.push({
        contactType: k.contactType,
        countryCode: k.countryCode,
        cellNumber: k.cellNumber,
      });
    });
    c.dateOfBirth = dateOfBirth;

    return c;
  }

  /**
   * A method to add new contact to Contacts array
   * @param contact Contact Object: { id: number;
  firstName: string;
  lastName: string;
  email: string;
  contactNumber: {
  countryCode: string;
  cellNumber: number;
  contactType: string;
}[];
  dateOfBirth: Date; }
   */
  addContacts(contact: Contact) {
    this.addressBook.add(contact);
    this.setContactDataToStorage();
  }

  /**
* A method to update existing contact into Contacts array
* @param contact Contact Object: { id: number;
firstName: string;
lastName: string;
email: string;
contactNumber: {
countryCode: string;
cellNumber: number;
contactType: string;
}[];
dateOfBirth: Date; }
*/
  updateContacts(contact: Contact) {
    this.addressBook.edit(contact);
    this.setContactDataToStorage();
  }

  /**
   * A method to delete existing contact from Contacts array
   * @param contactId Contact ID
   */
  deleteContacts(contactId: number) {
    this.addressBook.delete(contactId);
    this.setContactDataToStorage();
  }

  fetchContactList() {
    this.addressBook.contacts = getFromStorage<Contact[]>('contacts');
    return this.addressBook.contacts;
  }

  setContactDataToStorage() {
    setStorage<Contact[]>('contacts', this.addressBook.contacts);
  }
}
