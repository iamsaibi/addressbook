import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Contact } from 'src/app/model/contact.model';
import { ContactProcessingService } from 'src/app/services/contact-processing.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add-contacts',
  templateUrl: './add-contacts.component.html',
  styleUrls: ['./add-contacts.component.scss'],
})
export class AddContactsComponent implements OnInit {
  currentOperation: string = '';
  contactDataForm: FormGroup;
  contactName: string = 'Saibi Singhai';
  modal;
  contactTypes = ['Home', 'Office', 'Personal'];

  constructor(
    private contactProcessingSrv: ContactProcessingService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.createContactForm();
    this.modal = document.getElementById('myModal');
    this.activatedRoute.queryParamMap.subscribe((qParams) => {
      this.currentOperation = qParams.get('action') ? qParams.get('action').toString() : '';
    });
  }

  /**
   * Creates a new Form with validation for Contacts
   */
  createContactForm() {
    this.contactDataForm = new FormGroup({
      firstName: new FormControl('', [Validators.required, Validators.maxLength(25)]),
      lastName: new FormControl('', [Validators.required, Validators.maxLength(25)]),
      email: new FormControl('', [
        Validators.required,
        // Validators.email,
        Validators.maxLength(30),
        Validators.pattern(/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9]+\.[a-zA-Z]{2,}$/),
      ]),
      phone: new FormArray([]),
      dateOfBirth: new FormControl('', [Validators.required]),
    });
    (this.contactDataForm.get('phone') as FormArray).controls.push(this.phoneForm);
    console.log(this.contactDataForm);
  }

  log() {
    console.log(this.contactDataForm);
  }

  get phoneForm() {
    return new FormGroup({
      contactType: new FormControl('', Validators.required),
      countryCode: new FormControl('', Validators.required),
      cellNumber: new FormControl('', [
        Validators.required,
        Validators.pattern(/^[1-9]{1}[0-9]{9}$/),
      ]),
    });
  }

  addPhone() {
    this.phone.controls.push(this.phoneForm);
  }

  removePhone(index: number) {
    this.phone.removeAt(index);
  }

  /**
   * A method that is invoked immediately user clicks on the save button to save form data
   */
  saveContactDetails() {
    let data: Contact = this.contactProcessingSrv.consolidateData(
      this.firstName.value,
      this.lastName.value,
      this.email.value,
      this.phone.value,
      this.dateOfBirth.value
    );

    switch (this.currentOperation) {
      case 'add':
        this.contactProcessingSrv.addContacts(data);
        break;
      case 'edit':
        this.contactProcessingSrv.updateContacts(data);
        break;
    }
    this.modal.style.display = 'block';
  }

  closeDialog() {
    this.modal.style.display = 'none';
  }

  /**
   * Getter for Firstname
   */
  get firstName() {
    return this.contactDataForm.controls['firstName'];
  }

  /**
   * Getter for Lastname
   */
  get lastName() {
    return this.contactDataForm.controls['lastName'];
  }

  /**
   * Getter for Email
   */
  get email() {
    return this.contactDataForm.controls['email'];
  }

  get phone() {
    return this.contactDataForm.controls['phone'] as FormArray;
  }

  /**
   * Getter for Contact Type example Home, Office, Personal
   */
  get contactType() {
    return this.contactDataForm.controls['contactType'];
  }

  /**
   * Getter for Country Code
   */
  get countryCode() {
    return this.contactDataForm.controls['countryCode'];
  }

  /**
   * Getter for Contact Number
   */
  get cellNumber() {
    return this.contactDataForm.controls['cellNumber'];
  }

  /**
   * Getter for Date of birth
   */
  get dateOfBirth() {
    return this.contactDataForm.controls['dateOfBirth'];
  }
}
