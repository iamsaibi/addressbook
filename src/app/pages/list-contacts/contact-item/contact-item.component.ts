import { Component, OnInit, Input } from '@angular/core';
import { Contact } from 'src/app/model/contact.model';
import { ContactProcessingService } from 'src/app/services/contact-processing.service';

@Component({
  selector: 'app-contact-item',
  templateUrl: './contact-item.component.html',
  styleUrls: ['./contact-item.component.scss'],
})
export class ContactItemComponent implements OnInit {
  @Input() contactData: Contact;
  contactNumberCount: number;
  //emailCount: number;
  constructor(private contactProcessingSrv: ContactProcessingService) {}

  ngOnInit() {
    this.contactNumberCount = this.contactData['contactNumber'].length;
    //this.emailCount = this.contactData['email'].length;
  }

  deleteContact(id: number) {
    this.contactProcessingSrv.deleteContacts(id);
  }
}
