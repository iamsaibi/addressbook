import { Component, OnInit } from '@angular/core';
import { ContactProcessingService } from 'src/app/services/contact-processing.service';
import { Contact } from 'src/app/model/contact.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-contacts',
  templateUrl: './list-contacts.component.html',
  styleUrls: ['./list-contacts.component.scss'],
})
export class ListContactsComponent implements OnInit {
  contactList: Contact[];

  constructor(private contactProcesingSrv: ContactProcessingService, private router: Router) {}

  ngOnInit() {
    this.contactList = this.contactProcesingSrv.fetchContactList();
  }

  processContact(actionType: string, contactId?: number) {
    this.contactProcesingSrv.contactEditSubject.next(contactId);
    this.router.navigate([''], {
      queryParams: {
        action: actionType,
      },
    });
  }
}
