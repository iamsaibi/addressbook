import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddContactsComponent } from './pages/add-contacts/add-contacts.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ListContactsComponent } from './pages/list-contacts/list-contacts.component';
import { ContactItemComponent } from './pages/list-contacts/contact-item/contact-item.component';
@NgModule({
  declarations: [AppComponent, AddContactsComponent, ListContactsComponent, ContactItemComponent],
  imports: [ReactiveFormsModule, BrowserModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
